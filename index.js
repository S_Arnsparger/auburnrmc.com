var express = require("express")
var app = express()
var bodyParser = require("body-parser")
const fs = require('fs-extra')

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static("public"))
app.use(express.static("files"))

// =================================
// =========== ROUTES ==============
// =================================
app.get("/", function(req, res){
	res.render("home.ejs");
})

app.get("/team", function(req, res){
	res.render("team.ejs");
})

app.get("/team/:memberid", function(req, res){
	var member_id = req.params.member_id;
	res.render("member.ejs", {
		member_id: member_id
	})
})

app.get("/member-portal", function(req, res){
	res.render("member-portal.ejs");
})

app.get("*", function(req, res){
	res.send("STAR ROUTE");
})

app.listen(8080, "localhost", function() {
	console.log("Server is listening on localhost:8080");
	console.log("Hit ctrl+C to shutdown internal server.")
})
